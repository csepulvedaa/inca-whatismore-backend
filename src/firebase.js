// Import the functions you need from the SDKs you need
import { getApps, initializeApp } from "firebase/app";
import { getFirestore, addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { getAuth } from "firebase/auth";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

// Initialize Firebase

let firebaseApp;
let db;
let auth;
let v;

let defLog ={
    test_number: 0,
    time_last_display: 0,
    text_diary: [],
    csv_diary: [],
    text: new Blob(["# No test was executed before saving this text log. "], {type: 'text/plain'}),
    csv: new Blob(["# No test was executed before saving this csv log. "], {type: 'text/csv'})
}

function parseVersion(version){
    let sliced = version.split(".");
    return `${sliced[0]}.${sliced[1]}`;
}

export function startFirebase(version, firebaseConfig){
    v = parseVersion(version);
    if (!getApps().length) {
        firebaseApp = initializeApp(firebaseConfig);
    }
    else {
        firebaseApp = getApp();
        deleteApp(firebaseApp);
        firebaseApp = initializeApp(firebaseApp);
    }
    
    db = getFirestore(firebaseApp);
    auth = getAuth(firebaseApp);
    return {auth, db};
}

export function setDefaultLog(){
    localStorage.setItem('log', JSON.stringify(defLog));
    return defLog;
}

    // Functions
function enterInLocalLog(string) {
    let log = JSON.parse(localStorage.getItem('log'));
    if (log){
        log.text_diary = [...log.text_diary, string];
        log.text = new Blob(log.text_diary, {type: 'text/plain'});
        localStorage.setItem('log', JSON.stringify(log));
    } else {
        defLog.text_diary.push(string)
        localStorage.setItem('log', JSON.stringify(defLog));
    }
}

function enterInCSVLog(string) {
    let log = JSON.parse(localStorage.getItem('log'));
    if (log){
        log.csv_diary = [...log.csv_diary,"\n"+string];
        log.csv = new Blob(log.csv_diary, {type: 'text/csv'});
        localStorage.setItem('log', JSON.stringify(log));
    }else {
        defLog.csv_diary.push(string)
        localStorage.setItem('log', JSON.stringify(defLog));
    }
}

function saveLocally(event){
    let textEvent = JSON.stringify(event);
    enterInLocalLog(textEvent);
    enterInCSVLog(textEvent);
    console.log("Event saved in local log", event);
}

export async function SendToFirebase(user, teacher, learner, action, results) {
    let eventFb;
    try {
        const now = new Date();
        let event = {            
            date: now.toLocaleDateString(),
            time: now.toLocaleTimeString(),            
            teacher: teacher,
            subject: learner,
            action: `${action}`,
            details: {...results},
        }
        saveLocally(event);
        eventFb = {
            timestamp: serverTimestamp(),
            user: user ? user.displayName : "Anonymous",
            userId: user ? user.uid : "Anonymous",
            ...event,
        }
        await addDoc(collection(db, `Which-has-more${v}`), eventFb);
        console.log("Event sent to firebase!");
    } catch (error) {
        console.log("Error while sending event:", eventFb, "to firebase: ",error);
    }
}